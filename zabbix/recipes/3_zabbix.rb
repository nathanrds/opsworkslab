package "snmp" do
  action :install
end

bash "download_zabbix" do
  user "root"
  code <<-EOH
    wget http://ufpr.dl.sourceforge.net/project/zabbix/ZABBIX%20Latest%20Stable/2.2.2/zabbix-2.2.2.tar.gz -O /tmp/zabbix-2.2.2.tar.gz
    tar -zxf /tmp/zabbix-2.2.2.tar.gz -C /srv/
  EOH
end

bash "create_database" do
  user "root"
  ignore_failure true
  code <<-EOH
    mysql -uroot -pzabbix -e "create database zabbixdb;"
    mysql -uroot -pzabbix -e "grant all privileges on zabbixdb.* to zabbix@localhost identified by 'zabbix';"
    mysql -u zabbix -pzabbix zabbixdb < /srv/zabbix-2.2.2/database/mysql/schema.sql
    mysql -u zabbix -pzabbix zabbixdb < /srv/zabbix-2.2.2/database/mysql/images.sql
    mysql -u zabbix -pzabbix zabbixdb < /srv/zabbix-2.2.2/database/mysql/data.sql
  EOH
end

bash "install_zabbix" do
  user "root"
  code <<-EOH
   (cd /srv/zabbix-2.2.2/ && ./configure --enable-server --enable-agent --with-mysql --enable-ipv6 --with-snmp --with-libcurl --with-ssh2 && make install)
  EOH
end

cookbook_file "/etc/services" do
  source "services"
  mode 0644
  owner "root"
  group "root"
end

cookbook_file "/usr/local/etc/zabbix_agentd.conf" do
  source "zabbix_agentd.conf"
  mode 0644
  owner "root"
  group "root"
end

cookbook_file "/usr/local/etc/zabbix_server.conf" do
  source "zabbix_server.conf"
  mode 0644
  owner "root"
  group "root"
end

cookbook_file "/etc/init.d/zabbix-agent" do
  source "zabbix-agent"
  mode 0755
  owner "root"
  group "root"
end

cookbook_file "/etc/init.d/zabbix-server" do
  source "zabbix-server"
  mode 0755
  owner "root"
  group "root"
end

service "zabbix-server" do
 action :start
end

service "zabbix-agent" do
 action :start
end

bash "updaterc" do
  user "root"
  ignore_failure true
  code <<-EOH
   update-rc.d -f zabbix-server defaults
   update-rc.d -f zabbix-agent defaults
  EOH
end

bash "create_dir_apache" do
  user "root"
  ignore_failure true
  code <<-EOH
   mkdir /var/www/zabbix
   cp -a /srv/zabbix-2.2.2/frontends/php/* /var/www/zabbix/
   chown -R www-data:www-data /var/www/zabbix/
  EOH
end