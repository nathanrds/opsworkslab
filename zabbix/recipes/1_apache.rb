package "build-essential" do
  action :install
end

package "apache2" do
  action :install
end

package "php5" do
  action :install
end

package "php5-curl" do
  action :install
end

package "php5-dev" do
  action :install
end

package "php5-mysql" do
  action :install
end

package "php5-gd" do
  action :install
end

package "php5-xmlrpc" do
  action :install
end

package "openipmi" do
  action :install
end

package "libssh2-1" do
  action :install
end

package "libssh2-1-dev" do
  action :install
end

package "libssh2-php" do
  action :install
end

package "fping" do
  action :install
end

package "libcurl3" do
  action :install
end

package "libcurl4-openssl-dev" do
  action :install
end

package "libiksemel3" do
  action :install
end

package "libiksemel-dev" do
  action :install
end

package "libssh2-php" do
  action :install
end

cookbook_file "/etc/php5/apache2/php.ini" do
  source "php.ini"
  mode 0644
  owner "root"
  group "root"
end

service "apache2" do
 action :restart
end