package "libmysqld-dev" do
  action :install
end

package "libmysqld-pic" do
  action :install
end

package "libmysqlclient-dev" do
  action :install
end

group "zabbix" do
  action :create
end

user "zabbix" do
  action :create
  gid "zabbix"
  system true
end

bash "install_mysql_server" do
  user "root"
  ignore_failure true
  code <<-EOH
   (echo "mysql-server-5.5 mysql-server/root_password password zabbix" | debconf-set-selections && echo "mysql-server-5.5 mysql-server/root_password_again password zabbix" | debconf-set-selections && apt-get -y --force-yes install mysql-server-5.5)
  EOH
end